<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class GenreRepository extends EntityRepository
{
    public function createAlphabeticalQueryBuilder()
    {
        return $this->createQueryBuilder('genre')
            ->orderBy('genre.name', 'ASC');
    }
}