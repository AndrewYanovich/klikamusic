<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Track;
use Doctrine\ORM\EntityRepository;

class SingerRepository extends EntityRepository
{
    public function createAlphabeticalQueryBuilder()
    {
        return $this->createQueryBuilder('singer')
            ->orderBy('singer.name', 'ASC');
    }
}