<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Genre;
use AppBundle\Entity\Singer;
use AppBundle\Entity\Track;
use Doctrine\ORM\EntityRepository;

class MusicRepository extends EntityRepository
{
    /**
     * @param Genre $genre
     * @param Singer $singer
     * @return Track[]
     */
    public function findAllByParameters(Genre $genre, Singer $singer)
    {
        return $this->createQueryBuilder('track')
            ->andWhere('track.genre = :genre')
            ->setParameter('genre', $genre)
            ->andWhere('track.singer = :singer')
            ->setParameter('singer', $singer)
            ->addOrderBy('track.year', 'DESC')
            ->getQuery()
            ->execute();
    }

    public function findAllTracksForSinger(Singer $singer){
        return $this->createQueryBuilder('track')
            ->andWhere('track.singer = :singer')
            ->setParameter('singer', $singer)
            ->getQuery()
            ->execute();
    }

    public function findAllTracksForGenre(Genre $genre){
        return $this->createQueryBuilder('track')
            ->andWhere('track.genre = :genre')
            ->setParameter('genre', $genre)
            ->getQuery()
            ->execute();
    }

    public function sortBySinger($direction)
    {
        $qb = $this->createQueryBuilder('t')
            ->addSelect('singer')
            ->leftJoin('t.singer', 'singer')
            ->orderBy('t.name', $direction);
        return $qb->getQuery();
    }

    public function sortByGenre($direction)
    {
        $qb = $this->createQueryBuilder('t')
            ->addSelect('genre')
            ->leftJoin('t.genre', 'genre')
            ->orderBy('t.name', $direction);
        return $qb->getQuery();
    }
}