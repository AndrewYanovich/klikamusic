<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class SingerController extends Controller
{

    /**
     * @Route("/singers", name="singer_list")
     */
    public function listAction()
    {
        $em = $this->getDoctrine()->getManager();
        $singers = $em->getRepository('AppBundle:Singer')
            ->findAll();

        return $this->render('singer/list.html.twig', [
            'singers' => $singers,
        ]);
    }
}