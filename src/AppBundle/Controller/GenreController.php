<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Genre;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class GenreController extends Controller
{
    /**
     * @Route("/genres", name="genre_list")
     */
    public function listAction()
    {
        $em = $this->getDoctrine()->getManager();
        $genres = $em->getRepository('AppBundle:Genre')
            ->findAll();

        return $this->render('genre/list.html.twig', [
            'genres' => $genres,
        ]);
    }
}