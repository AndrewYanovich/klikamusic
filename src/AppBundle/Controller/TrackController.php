<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Genre;
use AppBundle\Entity\Singer;
use AppBundle\Entity\Track;
use AppBundle\Form\TrackfilterFormType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Knp\Component\Pager\Paginator;

class TrackController extends Controller
{
    /**
     * @Route("/tracks", name="track_list")
     */
    public function listAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        if($request->query->get('sort') == 'singer.name'){
            $tracks = $em->getRepository('AppBundle:Track')->sortBySinger($request->query->get('direction'));
        } else if($request->query->get('sort') == 'genre.name'){
            $tracks = $em->getRepository('AppBundle:Track')->sortByGenre($request->query->get('direction'));
        } else {
            $dql = "SELECT track FROM AppBundle:Track track";
            $tracks = $em->createQuery($dql);
        }

        #Filter Form
        $form = $this->createForm(TrackfilterFormType::class);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){

            $filterArray = [];
            foreach($form->getData() as $key => $value){
                if($value != null){
                    $filterArray[$key] = $value;
                }
            }

            $em = $this->getDoctrine()->getManager();
            $tracks = $em->getRepository('AppBundle:Track')
                ->findBy($filterArray);
        }

        /**
         * @var $paginator |Knp\Component\Pager\Paginator
         */
        $paginator = $this->get('knp_paginator');
        $result = $paginator->paginate(
            $tracks,
            $request->query->getInt('page', 1),
            $request->query->getInt('limit', 10)
        );

        $result->setSortableTemplate('utils/sortable_link.html.twig');

        return $this->render('track/list.html.twig', [
            'tracks' => $result,
            'trackfilterForm' => $form->createView()
        ]);
    }

}

