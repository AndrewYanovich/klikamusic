<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Genre;
use AppBundle\Form\GenreFormType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin")
 */
class GenreAdminController extends Controller
{
    /**
     * @Route("/genres", name="admin_genre_list")
     */
    public function indexAction()
    {
        $genres = $this->getDoctrine()
            ->getRepository('AppBundle:Genre')
            ->findAll();

        return $this->render('admin/genre/list.html.twig', [
            'genres' => $genres,
        ]);
    }

    /**
     * @Route("/genres/new", name="admin_genre_new")
     */
    public function newAction(Request $request)
    {
        $form = $this->createForm(GenreFormType::class);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
//            dump($form->getData());die;
            $genre = $form->getData();

            $em = $this->getDoctrine()->getManager();

            $checkIfUnique = $em->getRepository('AppBundle:Genre')
                ->findOneBy(['name'=> $genre->getName()]);
            if(!$checkIfUnique) {
                $em->persist($genre);
                $em->flush();
                $this->addFlash('success', 'Genre updated');
            } else {
                $this->addFlash('error', 'This genre already exist!');
            }

            return $this->redirectToRoute('admin_genre_list');
        }

        return $this->render('admin/genre/new.html.twig', [
            'genreForm' => $form->createView()
        ]);
    }

    /**
     * @Route("/genres/{id}/edit", name="admin_genre_edit")
     */
    public function editAction(Request $request, Genre $genre)
    {
        $form = $this->createForm(GenreFormType::class, $genre);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $genre = $form->getData();

            $em = $this->getDoctrine()->getManager();

            $checkIfUnique = $em->getRepository('AppBundle:Genre')
                ->findOneBy(['name'=> $genre->getName()]);
            if(!$checkIfUnique) {
                $em->persist($genre);
                $em->flush();
                $this->addFlash('success', 'Genre updated');
            } else {
                $this->addFlash('error', 'This genre already exist!');
            }

            return $this->redirectToRoute('admin_genre_list');
        }

        return $this->render('admin/genre/edit.html.twig', [
            'genreForm' => $form->createView()
        ]);
    }

    /**
     * @Route("/genres/{id}/delete", name="admin_genre_delete")
     */
    public function deleteAction(Genre $genre)
    {
        $em = $this->getDoctrine()->getManager();

        $tracksForGenre = $em->getRepository('AppBundle:Track')
            ->findAllTracksForGenre($genre);

        if(!$tracksForGenre){
            $em ->remove($genre);
            $em->flush();
            $this->addFlash('success', 'Genre deleted');
        } else {
            $this->addFlash('error', 'You can not deleted this genre while there are its songs in DB');
        }

        return $this->redirectToRoute('admin_genre_list');
    }
}