<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Singer;
use AppBundle\Form\SingerFormType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin")
 */
class SingerAdminController extends Controller
{
    /**
     * @Route("/singers", name="admin_singer_list")
     */
    public function indexAction()
    {
        $singers = $this->getDoctrine()
            ->getRepository('AppBundle:Singer')
            ->findAll();

        return $this->render('admin/singer/list.html.twig', [
            'singers' => $singers,
        ]);
    }

    /**
     * @Route("/singers/new", name="admin_singer_new")
     */
    public function newAction(Request $request)
    {
        $form = $this->createForm(SingerFormType::class);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $singer = $form->getData();

            $em = $this->getDoctrine()->getManager();

            $checkIfUnique = $em->getRepository('AppBundle:Singer')
                ->findOneBy(['name' => $singer->getName()]);
            if(!$checkIfUnique) {
                $em->persist($singer);
                $em->flush();
                $this->addFlash('success', 'Singer updated');
            } else {
                $this->addFlash('error', 'This singer already exist!');
            }

            return $this->redirectToRoute('admin_singer_list');
        }

        return $this->render('admin/singer/new.html.twig', [
            'singerForm' => $form->createView()
        ]);
    }

    /**
     * @Route("/singers/{id}/edit", name="admin_singer_edit")
     */
    public function editAction(Request $request, Singer $singer)
    {
        $form = $this->createForm(SingerFormType::class, $singer);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $singer = $form->getData();

            $em = $this->getDoctrine()->getManager();

            $checkIfUnique = $em->getRepository('AppBundle:Singer')
                ->findOneBy(['name'=> $singer->getName()]);
            if(!$checkIfUnique) {
                $em->persist($singer);
                $em->flush();
                $this->addFlash('success', 'Singer updated');
            } else {
                $this->addFlash('error', 'This singer already exist!');
            }



            return $this->redirectToRoute('admin_singer_list');
        }

        return $this->render('admin/singer/edit.html.twig', [
            'singerForm' => $form->createView()
        ]);
    }

    /**
     * @Route("/singers/{id}/delete", name="admin_singer_delete")
     */
    public function deleteAction(Singer $singer)
    {
        $em = $this->getDoctrine()->getManager();

        $tracksForSinger = $em->getRepository('AppBundle:Track')
            ->findAllTracksForSinger($singer);

        if(!$tracksForSinger){
            $em ->remove($singer);
            $em->flush();
            $this->addFlash('success', 'Singer deleted');
        } else {
            $this->addFlash('error', 'You can not deleted this singer while there are his songs in DB');
        }

        return $this->redirectToRoute('admin_singer_list');
    }
}