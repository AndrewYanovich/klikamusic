<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Track;
use AppBundle\Form\TrackFormType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin")
 */
class TrackAdminController extends Controller
{
    /**
     * @Route("/tracks", name="admin_track_list")
     */
    public function indexAction()
    {
        $tracks = $this->getDoctrine()
            ->getRepository('AppBundle:Track')
            ->findAll();

        return $this->render('admin/track/list.html.twig', [
            'tracks' => $tracks,
        ]);
    }

    /**
     * @Route("/tracks/new", name="admin_track_new")
     */
    public function newAction(Request $request)
    {
        $form = $this->createForm(TrackFormType::class);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
//            dump($form->getData());die;
            $track = $form->getData();

            $em = $this->getDoctrine()->getManager();

            $checkIfUnique = $em->getRepository('AppBundle:Track')
                ->findOneBy(['name'=> $track->getName()]);
            if(!$checkIfUnique) {
                $em->persist($track);
                $em->flush();
                $this->addFlash('success', 'Track updated');
            } else {
                $this->addFlash('error', 'This Track already exist!');
            }

            return $this->redirectToRoute('admin_track_list');
        }

        return $this->render('admin/track/new.html.twig', [
            'trackForm' => $form->createView()
        ]);
    }

    /**
     * @Route("/tracks/{id}/edit", name="admin_track_edit")
     */
    public function editAction(Request $request, Track $track)
    {
        $form = $this->createForm(TrackFormType::class, $track);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $track = $form->getData();

            $em = $this->getDoctrine()->getManager();

//            $checkIfUnique = $em->getRepository('AppBundle:Track')
//                ->findOneBy(['name'=> $track->getName()]);
//            if(!$checkIfUnique) {
                $em->persist($track);
                $em->flush();
                $this->addFlash('success', 'Track updated');
//            } else {
//                $this->addFlash('error', 'This Track already exist!');
//            }

            return $this->redirectToRoute('admin_track_list');
        }

        return $this->render('admin/track/edit.html.twig', [
            'trackForm' => $form->createView()
        ]);
    }

    /**
     * @Route("/tracks/{id}/delete", name="admin_track_delete")
     */
    public function deleteAction(Track $track)
    {
        $em = $this->getDoctrine()->getManager();
        $em ->remove($track);
        $em->flush();

        $this->addFlash('success', 'Track deleted');

        return $this->redirectToRoute('admin_track_list');
    }
}
