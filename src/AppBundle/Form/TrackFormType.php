<?php

namespace AppBundle\Form;

use AppBundle\Entity\Genre;
use AppBundle\Entity\Singer;
use AppBundle\Repository\GenreRepository;
use AppBundle\Repository\SingerRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TrackFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('singer', EntityType::class, [
                'placeholder' => 'Choose a Singer',
                'class' => Singer::class,
                'query_builder' => function(SingerRepository $repo){
                    return $repo->createAlphabeticalQueryBuilder();
                }
            ])
            ->add('genre', EntityType::class, [
                'placeholder' => 'Choose a Genre',
                'class' => Genre::class,
                'query_builder' => function(GenreRepository $repo){
                    return $repo->createAlphabeticalQueryBuilder();
                }
            ])
            ->add('year');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
           'data_class' => 'AppBundle\Entity\Track'
        ]);
    }

}
