<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MusicRepository")
 * @ORM\Table(name="track")
 */
class Track
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(type="string", unique=true)
     */
    private $name;

    /**
     * @Assert\Range(
     *     min = 1900,
     *     max = 2019,
     *     minMessage = "That is so old song. Add something newer than this.",
     *     maxMessage = "Hey! The year you wrote is not yet due!"
     * )
     * @Assert\NotBlank()
     * @ORM\Column(type="integer")
     */
    private $year;

    /**
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="Singer", inversedBy="tracks")
     * @ORM\JoinColumn(nullable=false)
     */
    private $singer;

    /**
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="Genre", inversedBy="tracks")
     * @ORM\JoinColumn(nullable=false)
     */
    private $genre;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param mixed $year
     */
    public function setYear($year)
    {
        $this->year = $year;
    }

    /**
     * @return mixed
     */
    public function getSinger()
    {
        return $this->singer;
    }

    /**
     * @param Singer $singer
     */
    public function setSinger(Singer $singer)
    {
        $this->singer = $singer;
    }

    /**
     * @return mixed
     */
    public function getGenre()
    {
        return $this->genre;
    }

    /**
     * @param Genre $genre
     */
    public function setGenre(Genre $genre)
    {
        $this->genre = $genre;
    }

    public function __toString()
    {
        return $this->getName();
    }
}